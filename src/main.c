#include "pmd_parser.h"
#include "vmd_parser.h"
#include "interpolation.h"
#include "frames.h"
#include "clock.h"
#include <stdlib.h>
#include "vjoy.h"
#include "menu.h"
#include <errno.h>

int main(int argc, char * argv[])
{
	FILE *fp;
	bone_store *store;
	int type, i;
	char model[20];
	uint32_t frame_count;
	vmdBoneKeyframe bone_frame;
	bone_data *bone;
	bone_key_t keyframe;
	float frame;
	struct timespec t_start;
	uint32_t elapsed, last_time, final_frame;
	unsigned int vjoy_device;
	bone_map_t vjoy_axis[8] = {0};
	menu_option_t * options;
	vjoy_state_t joy_state;


	/* Provided an argument? */
	if (argc == 1) {
		fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
		getchar();
		exit(1);
	}

	vjoy_device = vjoyInit();

	/*for (i = 0x0; i < 0xF000; i += 0x100) {
		vjoyWave(vjoy_device, i);
		Sleep(2);
	}*/

	/* Try to open the file */
	fp = fopen(argv[1], "rb");
	if (fp == NULL) {
		fprintf(stderr, "Error opening %s!\n", argv[1]);
		vjoyCleanup(vjoy_device);
		getchar();
		exit(2);
	}

	argv[1][strlen(argv[1]) - 3] = 'p';
	printf("New header: %s\n", argv[1]);

	store = pmd_read(argv[1]);

	printf("Name: %s\n", store->english_name);
	printf("----------------\n\n%s\n\n----------------\n", store->model_comment);

	if ((type = vmd_read_header(fp)) == 0) {
		fprintf(stderr, "VMD is not valid\n");
		fclose(fp);
		bs_free(store);
		vjoyCleanup(vjoy_device);
		getchar();
		exit(3);
	}

	vmd_model_name(type, model, fp);
	// printf("Model name: %s\n", model);

	if (strncmp(model, store->model_name, type * 10) != 0) {
		fprintf(stderr, "PMD and VMD files are not for the same model!\n");
		fclose(fp);
		bs_free(store);
		vjoyCleanup(vjoy_device);
		getchar();
		exit(4);
	}

	read_bytes(&frame_count, fp, 4);

	printf("Frames: %u\n", frame_count);

	final_frame = 0;

	for (i = 0; i < frame_count; ++i) {
		bone_frame = vmd_read_bone(fp);
		bone = bs_get(store, bone_frame.bone_name);
		if (bone == NULL) {
			printf("INVALID BONE AT %s\n", bone_frame.bone_name);
			continue;
		}

		keyframe = bk_create(bone_frame);

		bd_add(bone, keyframe);
		if (keyframe.frame_number > final_frame)
			final_frame = keyframe.frame_number;
	}

	fclose(fp);

	if ((options = calloc(store->entries, sizeof(menu_option_t))) == NULL) {
		perror("Failed to allocate menu options");
		vjoyCleanup(vjoy_device);
		getchar();
		exit(ENOMEM);
	}

	for (i = 0; i < store->entries; ++i) {
		//printf("%s: %u\n", store->bones[i]->english_name, (unsigned int) store->bones[i]->entries);
		options[i].name = store->bones[i]->english_name;
		options[i].index = i;
		options[i].bone = store->bones[i];
	}

	show_menu(options, vjoy_axis, store->entries);
	free(options);

	for (i = 0; i < 8; ++i) {
		joy_state.buttons[i] = FALSE;
		bone = vjoy_axis[i].bone;
		if (bone == NULL) {
			joy_state.axis[i] = 0;
		} else if (bone->frames != 0) {
			frame = bk_interpolate(0, bone->frames[0], bone->frames[0], vjoy_axis[i].axis);
			joy_state.axis[i] = (frame * 0x4000) + 0x4000;
		}
	}

	vjoySet(vjoy_device, joy_state);

	printf("Press any key to begin\n");
	getchar();

	printf("3\n");
	Sleep(1000);
	printf("2\n");
	Sleep(1000);
	printf("1\n");
	Sleep(1000);
	printf("GO\n");

	clock_gettime(CLOCK_MONOTONIC, &t_start);
	last_time = 1000;
	while ((elapsed = elapsed_frames(t_start)) <= final_frame) {
		if (last_time != elapsed)
			last_time = elapsed;
		else
			continue;

		//for (i = 0; i < store->entries; ++i) {
		for (i = 0; i < 8; ++i) {
			//bone = store->bones[i];
			bone = vjoy_axis[i].bone;
			if (bone == NULL)
				break;


			if (bone->entries == bone->index) // Out of frames
				continue;
			else if (bone->curr_frame > elapsed) // Not yet hit first frame
				continue;
			else if (bone->next_frame <= elapsed) { // Hit next frame
				printf("%s: %u -> %u\n", bone->english_name, bone->curr_frame, bone->next_frame);
				bone->curr_frame = bone->next_frame;
				if  (bone->entries >= ++bone->index) {
					bone->next_frame = bone->frames[bone->index].frame_number;
				}
			}

			// Interpolating between frames
			frame = bk_interpolate(elapsed, bone->frames[bone->index-1], bone->frames[bone->index], vjoy_axis[i].axis);

			if (bone->index != bone->entries)
				printf("%d -> %d\n", bone->frames[bone->index-1].frame_number, bone->frames[bone->index].frame_number);
			else
				printf("%d -> EOF\n", bone->frames[bone->index-1].frame_number);

			// Output frame
			joy_state.axis[i] = (frame * 0x4000) + 0x4000;
		}
		if (elapsed % 30 == 0)
			joy_state.buttons[0] = TRUE;
		else
			joy_state.buttons[0] = FALSE;
		vjoySet(vjoy_device, joy_state);
		printf("%u\n", elapsed);
	}

	bs_free(store);
	vjoyCleanup(vjoy_device);

	return 0;
}
