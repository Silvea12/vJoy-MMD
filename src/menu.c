#include "menu.h"
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

void show_menu(const menu_option_t * options, bone_map_t * joy_map, const uint16_t entries)
{
	size_t i;
	int bone_input = -1;
	char axis_input = '\0';
	char c;

	printf("\n");

	for (i = 0; i < entries; ++i)
		printf("%d: %s (%d)\n", options[i].index + 1, options[i].name, options[i].bone->entries);

	printf("\nWhen picking an axis, format input as \"[id][y/p/r]\"\n");
	printf("'y' being yaw, 'p' being pitch and 'r' being roll.\n");
	printf("Example: \"20y\"\n");

	for (i = 0; i < 8; ++i) {
		printf("\nChoose axis %d: ", i + 1);
		fflush(stdout);

		scanf("%d%c", &bone_input, &axis_input);
		while (bone_input != 0 &&
			((bone_input < 0 || bone_input > entries) ||
			(axis_input != 'p' && axis_input != 'y' && axis_input != 'r'))) {
			printf("Error!\nChoose axis %d: ", i + 1);
			fflush(stdout);

			do {
				c = getchar();
			} while (!isdigit(c));
			ungetc(c, stdin);
			scanf("%d%c", &bone_input, &axis_input);
		}

		if (bone_input == 0) {
			joy_map[i].bone = NULL;
			break;
		} else {
			joy_map[i].bone = options[bone_input-1].bone;
			switch (axis_input) {
				case 'y':
					joy_map[i].axis = YAW;
					break;

				case 'p':
					joy_map[i].axis = PITCH;
					break;

				case 'r':
					joy_map[i].axis = ROLL;
					break;

				default:
					fprintf(stderr, "Error: invalid joymap! %d %c\n", bone_input, axis_input);
					exit(1);
			}
			printf("Axis %d mapped to %s, channel %s (%d)\n",
				bone_input,
				options[bone_input-1].name,
				(joy_map[i].axis == YAW ? "yaw" : joy_map[i].axis == PITCH ? "pitch" : "roll"),
				options[bone_input-1].bone->entries);
		}
	}

}
