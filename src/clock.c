#include "clock.h"

typedef struct timespec timespec;

timespec diff(const timespec start, const timespec end)
{
	timespec ret;
	if (end.tv_nsec < start.tv_nsec) {
		ret.tv_sec = end.tv_sec - start.tv_sec - 1;
		ret.tv_nsec = 1e9 + end.tv_nsec - start.tv_nsec;
	} else {
		ret.tv_sec = end.tv_sec - start.tv_sec;
		ret.tv_nsec = end.tv_nsec - start.tv_nsec;
	}
	return ret;
}

uint32_t elapsed_frames(const timespec t_start)
{
	timespec t_now, t_diff;
	const unsigned int rate = 1e9/30;

	clock_gettime(CLOCK_MONOTONIC, &t_now);
	t_diff = diff(t_start, t_now);

	return (t_diff.tv_sec * 1e9 + t_diff.tv_nsec) / rate;
}
/*
int main(int argc, char * argv[])
{
	timespec t1, t2, td;
	unsigned int ticks;
	unsigned int passed;
	const unsigned int rate = 1e9/30;

	ticks = 0;

	clock_gettime(CLOCK_MONOTONIC, &t1);
	for (;;) {
		clock_gettime(CLOCK_MONOTONIC, &t2);
		td = diff(t1, t2);
		passed = (td.tv_sec * 1e9 + td.tv_nsec) / rate;
		if (passed > ticks) {
			ticks = passed;
			printf("*TICK* %u\n", ticks);
		} else {
			//printf("%f\n", (double) (td.tv_sec * 1e9 + td.tv_nsec) / rate);
		}
	}

	return 0;
}
*/
