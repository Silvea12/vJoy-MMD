#pragma once

#include <stdio.h>
#include <stdint.h>
#include <string.h>

typedef unsigned char byte;

/* Read x bytes from a file */
void read_bytes(void * out, FILE *fp, const int bytes);

/* Read 1 int from a file */
uint32_t read_int(FILE *fp);
