#pragma once

#include "vmd_types.h"

/* Bone keyframe (for storage) */
typedef struct _bone_key_t_ {
	float w, x, y, z;
	uint32_t frame_number;
} bone_key_t;

/* Bone frame (for output) */
typedef struct _bone_frame_t_ {
	float pitch, yaw, roll;
} bone_frame_t;

/* Bone axis (for output) */
typedef enum _bone_axis_t_ {
	NONE = 0,
	YAW = 1,
	PITCH = 2,
	ROLL = 3
} bone_axis_t;

/* Converts a vmdBoneKeyframe to a bone_key_t */
bone_key_t bk_create(const vmdBoneKeyframe kf);

/* Interpolates between key1 and key2
   Expects key1 to occur before key2, or will not correctly interpolate */
float bk_interpolate(const uint32_t frame_number, const bone_key_t key1, const bone_key_t key2, const bone_axis_t axis);
