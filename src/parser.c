#include "parser.h"
#include <stdlib.h>

void read_bytes(void * out, FILE *fp, const int bytes)
{
	char buf[bytes];
	int i;

	for (i = 0; i < bytes; ++i)
	{
		buf[i] = fgetc(fp);
		if (feof(fp) != 0 || ferror(fp) != 0)
		{
			fprintf(stderr, "Error with fp!\n");
			getchar();
			exit(4);
		}
	}

	memcpy(out, buf, bytes);
}

uint32_t read_int(FILE *fp)
{
	uint32_t out; 

	read_bytes(&out, fp, 4);

	return out;
}
