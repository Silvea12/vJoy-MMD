#include "frames.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

bone_store *bs_create(const char *name,
	const char *comment, const uint16_t size)
{
	bone_store *store;

	if (size < 1)
		return NULL;

	if ((store = malloc(sizeof(bone_store))) == NULL)
		return NULL;

	if ((store->bones = calloc(size, sizeof(bone_data *))) == NULL) {
		free(store);
		return NULL;
	}

	store->size = size;
	store->entries = 0;

	memcpy(store->model_name, name, 20);
	memcpy(store->model_comment, comment, 256);

	return store;
}

/* Free bone data */
void bd_free(bone_data *bone)
{
	//size_t i;

	//for (i = 0; i < bone->entries; ++i)
		//free(bone->frames[i]);

	free(bone->frames);

	free(bone);
}

void bs_free(bone_store *store)
{
	size_t i;

	for (i = 0; i < store->entries; ++i)
		bd_free(store->bones[i]);

	free(store->bones);

	free(store);
}

bone_data *bs_get(bone_store *store, const char *name)
{
	size_t i;

	for (i = 0; i < store->entries; ++i)
		if (memcmp(store->bones[i]->name, name, 15) == 0)
			return store->bones[i];

	return NULL;
}

bone_data *bd_create(const char *name, const char *english_name)
{
	bone_data *bone;

	if ((bone = malloc(sizeof(bone_data))) == NULL)
		return NULL;

	if ((bone->frames = calloc(1, sizeof(bone_key_t))) == NULL) {
		free(bone);
		return NULL;
	}

	bone->size = 1;
	bone->entries = 0;
	bone->index = 0;
	bone->curr_frame = 0;
	bone->next_frame = 0;

	memcpy(bone->name, name, 20);
	if (english_name != NULL)
		memcpy(bone->english_name, english_name, 20);

	return bone;
}

void bs_add(bone_store *store, bone_data *bone)
{
	store->bones[store->entries++] = bone;
}

int cmpframe(const void * bone1, const void * bone2)
{
	return ((const bone_key_t *)bone1)->frame_number > ((const bone_key_t *)bone2)->frame_number ? 1
	: ((const bone_key_t *)bone1)->frame_number < ((const bone_key_t *)bone2)->frame_number ? -1 : 0;
}

void bd_add(bone_data *bone, bone_key_t frame)
{
	size_t i;

	if (bone->entries == bone->size) {
		if ((bone->frames = (bone_key_t *) realloc(bone->frames, (sizeof(bone_key_t) * bone->size) << 1)) == NULL) {
			perror("Failed to increase bone frame storage");
			getchar();
			exit(ENOMEM);
		}
		bone->size = bone->size << 1;
	}

	if (bone->entries == 0) {
		bone->curr_frame = frame.frame_number;
		bone->next_frame = frame.frame_number;
	}

	if (bone->entries > 1 && bone->frames[bone->entries-1].frame_number > frame.frame_number) {
		++bone->entries;
		for (i = bone->entries-1; i > 0 && bone->frames[i-1].frame_number > frame.frame_number; --i)
			bone->frames[i] = bone->frames[i-1];

		bone->frames[i] = frame;
	} else {
		bone->frames[bone->entries++] = frame;
	}

}
