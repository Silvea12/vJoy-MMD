#pragma once

#include "vjoy.h"

/* Menu option storage */
typedef struct _menu_option_t_ {
	char * name;
	int index;
	bone_data * bone;
} menu_option_t;

/* Shows axis mapping menu */
void show_menu(const menu_option_t * options, bone_map_t * joy_map, const uint16_t entries);
