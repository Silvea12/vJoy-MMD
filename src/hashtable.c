#include "hashtable.h"
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
char * strndup (const char *s, size_t n)
{
	char *result;
	size_t len = strlen (s);

	if (n < len)
		len = n;

	result = (char *) malloc (len + 1);
	if (!result)
		return 0;

	result[len] = '\0';
	return (char *) memcpy (result, s, len);
}
#endif

hashtable_t *ht_create(size_t size, const char *name)
{
	hashtable_t *table = NULL;

	if (size < 1)
		return NULL;

	if ((table = malloc(sizeof(hashtable_t))) == NULL)
		return NULL;

	if ((table->table = calloc(size, sizeof(hashentry_t *))) == NULL) {
		free(table);
		return NULL;
	}

	memcpy(table->name, name, 20);
	memcpy(table->real_name, name, 20);

	table->size = size;
	table->entries = 0;

	return table;
}

void ht_free(hashtable_t *table)
{
	hashentry_t *entry;
	bone_key_t *frame;
	size_t i, j;

	for (i = 0; i < table->size; ++i) {
		entry = table->table[i];
		if (entry != NULL) {
			free(entry->key);
			free(entry->english_name);
			for (j = 0; j < entry->size; ++j) {
				frame = entry->frames[j];
				if (frame != NULL)
					free(frame);
			}
			free(entry->frames);
			free(entry);
		}
	}

	free(table->table);
	free(table);
}

unsigned int ht_hash(hashtable_t *table, const char *key)
{
	unsigned int hash;
	size_t i;

	hash = 0;

	for (i = 0; i < 20; ++i)
		hash = *(key+i) + (hash << 5) - hash;

	return hash % table->size;
}

hashentry_t *ht_lookup(hashtable_t *table, const char *key)
{
	hashentry_t *entry;
	unsigned int hash;

	hash = ht_hash(table, key);
	for (entry = table->table[hash]; entry != NULL; entry = entry->next) {
		if (memcmp(key, entry->key, 20) == 0)
			return entry;
	}

	return NULL;
}

hashentry_t *ht_add(hashtable_t *table, const char *key, const char *name)
{
	hashentry_t *entry;
	unsigned int hash;

	hash = ht_hash(table, key);


	if (table->table[hash] != NULL) {
		for (entry = table->table[hash]; entry != NULL; entry = table->table[(hash + 1) % table->size]) {
			if (memcmp(key, entry->key, 20) == 0)
				return entry;
			else
				++hash;
		}
		hash = (hash + 1) % table->size;
	}

	if ((entry = malloc(sizeof(hashentry_t))) != NULL) {
		entry->key = malloc(sizeof(entry->key) * 20);
		entry->english_name = malloc(sizeof(entry->english_name) * 20);
		memcpy(entry->key, key, 20);
		memcpy(entry->english_name, name, 20);
		entry->entries = 0;
		entry->size = 1;
		if ((entry->frames = calloc(1, sizeof(bone_key_t *))) == NULL) {
			free(entry->key);
			free(entry->english_name);
			free(entry);
			return NULL;
		}
		table->table[hash] = entry;
		if (table->table[(hash - 1) % table->size] != NULL)
			table->table[(hash - 1) % table->size]->next = entry;
		table->entries = table->entries + 1;
	}

	return entry;
}
