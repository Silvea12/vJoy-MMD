#pragma once

#include <stdint.h>
#include "parser.h"

typedef struct _vmdBoneKeyframe_ {
	char bone_name [15];
	uint32_t frame_number;
	float pos_x, pos_y, pos_z;
	float rot_x, rot_y, rot_z, rot_w;
	byte interpolation_data[64];
} vmdBoneKeyframe;

typedef struct _vmdFaceKeyframe_ {
	char face_name [15];
	uint32_t frame_number;
	float weight;
} vmdFaceKeyframe;
