#pragma once

#include "frames.h"
#include <windows.h>

/* Joystick state */
typedef struct _vjoy_state_t_ {
	int axis[8];
	BOOL buttons[8];
} vjoy_state_t;

/* Initialize vJoy */
int vjoyInit();

/* Clean up vJoy */
void vjoyCleanup(const int iInterface);

/* Do the wave with vJoy axis+buttons */
void vjoyWave(const int iInterface, const int scrollPos);

/* Set vJoy state */
void vjoySet(const int iInterface, const vjoy_state_t state);
