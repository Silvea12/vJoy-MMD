#pragma once

#include "interpolation.h"

/* Hash table entry */
typedef struct _hashentry_t_ {
	char *key;
	char *english_name;
	size_t entries, size;
	struct _bone_key_t_ **frames;
	struct _hashentry_t_ *next;
} hashentry_t;

/* Hash table */
typedef struct _hashtable_t_ {
	char name[20];
	char real_name[20];
	char comment[256];
	size_t size;
	size_t entries;
	hashentry_t **table;
} hashtable_t;


/* Create a hash table */
hashtable_t *ht_create(size_t size, const char *model_name);

/* Free the hash table */
void ht_free(hashtable_t *table);

/* Hash a value */
unsigned int ht_hash(hashtable_t *table, const char *key);

/* Lookup a value in the hash table */
hashentry_t *ht_lookup(hashtable_t *table, const char *key);

/* Set a value in the hash table */
hashentry_t *ht_add(hashtable_t *table, const char *key, const char *name);
