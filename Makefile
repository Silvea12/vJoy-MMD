TARGET		= parser.exe

CC			= x86_64-w64-mingw32-gcc
CFLAGS		= -O3 -Wall
LINKER		= x86_64-w64-mingw32-gcc -o
LFLAGS		= -O3 -Wall -lm lib/vjoy/amd64/vJoyInterface.dll
CSTRIP		= x86_64-w64-mingw32-strip

SRCDIR		= src
OBJDIR		= obj
BINDIR		= bin

SOURCES 	:= $(wildcard $(SRCDIR)/*.c)
INCLUDES	:= $(wildcard $(SRCDIR)/*.h)
OBJECTS		:= $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
rm			= rm -f

$(BINDIR)/$(TARGET): $(OBJECTS)
	$(LINKER) $@ $(LFLAGS) $(OBJECTS)

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

.PHONEY: strip
strip: $(BINDIR)/$(TARGET)
	$(CSTRIP) $(BINDIR)/$(TARGET)

.PHONEY: clean
clean:
	$(rm) $(OBJECTS) $(BINDIR)/$(TARGET)

.PHONEY: valgrind
valgrind: $(BINDIR)/$(TARGET)
	valgrind --leak-check=full --track-origins=yes $(BINDIR)/$(TARGET) res/Model/Miku_Hatsune_Ver2.vmd #res/Golden\ Freddy.vmd # >/dev/null
